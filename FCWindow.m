#import <UIKit/UIKit.h>
#import <objc/runtime.h>
#import "FCViewController.h"
#import "FCWindow.h"
#import <rocketbootstrap/rocketbootstrap.h>
#import <FCChatHeads/FCChatHeads.h>
#import <AppSupport/CPDistributedMessagingCenter.h>

@implementation FCWindow

- (instancetype)init {
  HBLogDebug(@"init");
  self = [super initWithFrame:[[UIScreen mainScreen] bounds]];
  if (self != nil){
  	[self setHidden:NO];
    [self setWindowLevel:UIWindowLevelAlert];
    UIViewController* viewcontroller = [[FCViewController sharedInstance] init];
    [self setRootViewController:viewcontroller];
  	[self setBackgroundColor:[UIColor clearColor]];
  	[self setUserInteractionEnabled:YES];
  }
  return self;
}

-(void)makeKeyAndVisible {
    [super makeKeyAndVisible];
    return;
}
-(bool)_shouldCreateContextAsSecure {
    return 0x0;
}

-(UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
  UIView *hitTestResult = [super hitTest:point withEvent:event];
  if (([ChatHeadsController isExpanded] == false) && ([hitTestResult isKindOfClass:[FCChatHead class]] == false)) {
    return nil;
  }
  return hitTestResult;
}

+ (instancetype)sharedInstance {
  static dispatch_once_t p = 0;
  __strong static id _sharedSelf = nil;
  dispatch_once(&p, ^{
    _sharedSelf = [[self alloc] init];
  });
  return _sharedSelf;
}

@end
