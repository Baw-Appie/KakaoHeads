#import <FCChatHeads/FCChatHeads.h>
#import <KakaoTalk/exported.h>
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <objc/runtime.h>
#import "FCViewController.h"
#import "FCWindow.h"
#import <rocketbootstrap/rocketbootstrap.h>
#import <AppSupport/CPDistributedMessagingCenter.h>
#import <mach-o/dyld.h>
#import <dlfcn.h>

extern "C" CFPropertyListRef MGCopyAnswer(CFStringRef property);

@interface UIWindow (Private)
- (void)_setSecure:(BOOL)secure;
@end

int licensed = 0;

%hook SpringBoard
-(void)applicationDidFinishLaunching:(id)application {
  HBLogDebug(@"Respring...");
  %orig;
  [[objc_getClass("NSDistributedNotificationCenter") defaultCenter] addObserver:self selector:@selector(KakaoHeadsShowAlert:) name:@"com.rpgfarm.kakaoheadsplus.showalert" object:nil];
  UIWindow* window = [FCWindow sharedInstance];
  [window makeKeyAndVisible];
}

%new
-(void)KakaoHeadsShowAlert:(NSNotification *)notification {
  HBLogDebug(@"Licensed %d", licensed);
  UIWindow *displayAlertWindow = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
  displayAlertWindow.windowLevel = UIWindowLevelAlert+1;
  displayAlertWindow.rootViewController = [UIViewController new];
  if ([displayAlertWindow respondsToSelector:@selector(_setSecure:)]) [displayAlertWindow _setSecure:YES];
  UIAlertController * alert =   [UIAlertController alertControllerWithTitle:@"Unable to load KakaoHeads." message:@"KakaoHeads is currently unavailable, please try again in a moment." preferredStyle:UIAlertControllerStyleAlert];
  UIAlertAction * ok = [UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault	handler:^(UIAlertAction * action) { [alert dismissViewControllerAnimated:YES completion:nil]; displayAlertWindow.hidden = YES; }];
  [alert addAction:ok];

  [displayAlertWindow makeKeyAndVisible];
  [displayAlertWindow.rootViewController presentViewController:alert animated:YES completion:nil];
}
%end

%hook SBHomeHardwareButton
-(void)singlePressUp:(id)arg1 {
  if([ChatHeadsController isExpanded]) [ChatHeadsController collapseChatHeads];
  else %orig;
}
%end
%hook SBDeckSwitcherViewController
-(void)viewDidAppear:(BOOL)arg1 {
  if([ChatHeadsController isExpanded]) [ChatHeadsController collapseChatHeads];
  else %orig;
}
%end


void requestChatHead(NSDictionary *dic) {
  CPDistributedMessagingCenter *c = [CPDistributedMessagingCenter centerNamed:@"com.rpgfarm.kakaoheadsplus"];
  rocketbootstrap_distributedmessagingcenter_apply(c);
  [c sendMessageName:@"receiver" userInfo:dic];
}

void processData(NSString* package, id chatid) {
  HBLogDebug(@"Checking Package %@", package);
  if([package isEqualToString:@"com.iwilab.KakaoTalk"]) {
    NSNumber *chatID = (NSNumber *)chatid;
    UIImage *profileimg = [[objc_getClass("Chat") fetchByID:chatID] thumbailForWatch];
    id unreadCount = [[objc_getClass("Chat") fetchByID:chatID] displayUnreadCount];
    id title = [[objc_getClass("Chat") fetchByID:chatID] displayTitle];
    HBLogDebug(@"requestChatHead.... %@", chatID);
    requestChatHead([NSDictionary dictionaryWithObjectsAndKeys:chatID, @"chatID", UIImagePNGRepresentation(profileimg), @"profileimg", unreadCount, @"unreadCount", package, @"package", title, @"title", nil]);
  }
}

void notificationReceived(NSString* application, id chatid) {
  if(licensed == 0) {
    if (![[NSFileManager defaultManager] fileExistsAtPath:@"/var/lib/dpkg/info/com.rpgfarm.kakaoheads.list"]){
      licensed = 2;
      [[objc_getClass("NSDistributedNotificationCenter") defaultCenter] postNotificationName:@"com.rpgfarm.kakaoheadsplus.showalert" object:nil userInfo:nil];
    } else {
      NSString *udid = (__bridge NSString *)MGCopyAnswer(CFSTR("UniqueDeviceID"));
      HBLogDebug(@"Getting UDID, %@", udid);
      NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
      [request setHTTPMethod:@"GET"];
      [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://license.rpgfarm.com/checkTweakLicense?package=com.rpgfarm.kakaoheads&udid=%@", udid]]];
      [request setTimeoutInterval:3.0];

      NSHTTPURLResponse *responseCode = nil;
      NSError *error;
      NSData *oResponseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&responseCode error:&error];
      NSString *res = [[NSString alloc] initWithData:oResponseData encoding:NSUTF8StringEncoding];
      if(([responseCode statusCode] != 200 || ![res isEqualToString:@"1"]) && error == nil){
        licensed = 2;
        [[objc_getClass("NSDistributedNotificationCenter") defaultCenter] postNotificationName:@"com.rpgfarm.kakaoheadsplus.showalert" object:nil userInfo:nil];
      } else {
        licensed = 1;
        processData(application, chatid);
      }
    }
  }
  if(licensed == 1) processData(application, chatid);
}

%hook LocoPushNotification
+(void)showWithMessage:(void *)arg2 chatId:(unsigned long long)arg3 logId:(unsigned long long)arg4 soundName:(void *)arg5 userInfo:(void *)arg6 category:(void *)arg7 {
  %orig;
  notificationReceived(@"com.iwilab.KakaoTalk", [NSNumber numberWithUnsignedLongLong:arg3]);
}
%end

%hook TalkAppDelegate
-(void)showFirstView {
  %orig;
  CPDistributedMessagingCenter *c = [CPDistributedMessagingCenter centerNamed:@"com.rpgfarm.kakaoheadsplus"];
  rocketbootstrap_distributedmessagingcenter_apply(c);
  [c sendMessageName:@"opener" userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"com.iwilab.KakaoTalk", @"package", nil]];
	[[objc_getClass("NSDistributedNotificationCenter") defaultCenter] addObserver:self selector:@selector(OpenThread:) name:@"com.rpgfarm.kakaoheadsplus.kakaotalk.opener" object:nil];
	[[objc_getClass("NSDistributedNotificationCenter") defaultCenter] addObserver:self selector:@selector(StartVoiceCall:) name:@"com.rpgfarm.kakaoheadsplus.kakaotalk.voicecall" object:nil];
}

%new
-(void)OpenThread:(NSNotification *)notification {
  NSString *chatID = [notification.userInfo objectForKey:@"chatID"];
  NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
  f.numberStyle = NSNumberFormatterDecimalStyle;
  NSNumber *cid = [f numberFromString:chatID];
  id chat = [objc_getClass("Chat") fetchByID:cid];
  [objc_getClass("Chat") openChatRoomWithChat:chat];
  [[objc_getClass("AppNavigationController") defaultController] openChatting:chat];
  HBLogDebug(@"Opening ChatRoom.. %@", cid);
}
%new
-(void)StartVoiceCall:(NSNotification *)notification {
  NSString *chatID = [notification.userInfo objectForKey:@"chatID"];
  NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
  f.numberStyle = NSNumberFormatterDecimalStyle;
  NSNumber *cid = [f numberFromString:chatID];
  id chat = [objc_getClass("Chat") fetchByID:cid];
  [[objc_getClass("KakaoTalk.VTManager") shared] startWithChat:chat type:TRUE completion:nil];
  HBLogDebug(@"Starting VoiceCall.. %@", cid);
}
%end


%ctor {
  if(![[NSFileManager defaultManager] fileExistsAtPath:@"/Library/MobileSubstrate/DynamicLibraries/MessageHeadsXI.dylib"]) {
    %init();
    HBLogDebug(@"KakaoHeads by Baw Appie Started.");
  }
}
