#define __is__iOS9__ [[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0

@interface FBSSceneSettings : NSObject

//@property (nonatomic,copy,readonly) FBSDisplayIdentity * displayIdentity;
//@property (nonatomic,copy,readonly) FBSDisplayConfiguration * displayConfiguration;              //@synthesize displayConfiguration=_displayConfiguration - In the implementation block
@property (nonatomic,readonly) CGRect frame;                                                     //@synthesize frame=_frame - In the implementation block
@property (nonatomic,readonly) double level;                                                     //@synthesize level=_level - In the implementation block
@property (nonatomic,readonly) long long interfaceOrientation;                                   //@synthesize interfaceOrientation=_interfaceOrientation - In the implementation block
@property (getter=isBackgrounded,nonatomic,readonly) BOOL backgrounded;                          //@synthesize backgrounded=_backgrounded - In the implementation block
@property (nonatomic,copy,readonly) NSArray * occlusions;                                        //@synthesize occlusions=_occlusions - In the implementation block
@property (readonly) unsigned long long hash;
@property (readonly) Class superclass;
@property (copy,readonly) NSString * description;
@property (copy,readonly) NSString * debugDescription;
+(BOOL)_isMutable;
+(id)settings;
-(id)debugDescriptionWithMultilinePrefix:(id)arg1 ;
-(id)transientLocalSettings;
-(id)_descriptionBuilderWithMultilinePrefix:(id)arg1 debug:(BOOL)arg2 ;
-(id)ignoreOcclusionReasons;
-(NSArray *)occlusions;
-(BOOL)isIgnoringOcclusions;
-(id)succinctDescription;
-(id)descriptionWithMultilinePrefix:(id)arg1 ;
-(id)descriptionBuilderWithMultilinePrefix:(id)arg1 ;
-(id)succinctDescriptionBuilder;
-(id)init;
-(void)dealloc;
-(BOOL)isEqual:(id)arg1 ;
-(CGRect)bounds;
-(unsigned long long)hash;
-(NSString *)description;
-(NSString *)debugDescription;
-(id)copyWithZone:(NSZone*)arg1 ;
-(long long)interfaceOrientation;
-(CGRect)frame;
-(id)mutableCopyWithZone:(NSZone*)arg1 ;
-(id)initWithSettings:(id)arg1 ;
-(BOOL)isBackgrounded;
-(BOOL)isOccluded;
-(double)level;
-(id)keyDescriptionForSetting:(unsigned long long)arg1 ;
-(id)valueDescriptionForFlag:(long long)arg1 object:(id)arg2 ofSetting:(unsigned long long)arg3 ;
-(id)otherSettings;
@end

@interface FBSceneHostManager : NSObject

@property (nonatomic,copy) NSString * identifier;                                         //@synthesize identifier=_identifier - In the implementation block
@property (getter=isInvalidated,nonatomic,readonly) BOOL invalidated;                     //@synthesize invalidated=_invalidated - In the implementation block
@property (nonatomic,readonly) long long contentState;                                    //@synthesize contentState=_contentState - In the implementation block
@property (assign,nonatomic) BOOL defaultClippingDisabled;                                //@synthesize defaultClippingDisabled=_defaultClippingDisabled - In the implementation block
@property (assign,nonatomic) CGAffineTransform defaultHostViewTransform;                  //@synthesize defaultHostViewTransform=_defaultHostViewTransform - In the implementation block
@property (nonatomic,copy) UIColor * defaultBackgroundColorWhileHosting;
@property (nonatomic,copy) UIColor * defaultBackgroundColorWhileNotHosting;
@property (assign,nonatomic) unsigned long long defaultHostedLayerTypes;                  //@synthesize defaultHostedLayerTypes=_defaultHostedLayerTypes - In the implementation block
@property (assign,nonatomic) unsigned long long defaultRenderingMode;                     //@synthesize defaultRenderingMode=_defaultRenderingMode - In the implementation block
@property (nonatomic,copy) NSString * defaultMinificationFilterName;                      //@synthesize defaultMinificationFilterName=_defaultMinificationFilterName - In the implementation block
@property (readonly) unsigned long long hash;
@property (readonly) Class superclass;
@property (copy,readonly) NSString * description;
@property (copy,readonly) NSString * debugDescription;
-(void)setDefaultClippingDisabled:(BOOL)arg1 ;
-(void)setDefaultHostViewTransform:(CGAffineTransform)arg1 ;
-(void)setDefaultHostedLayerTypes:(unsigned long long)arg1 ;
-(void)setDefaultRenderingMode:(unsigned long long)arg1 ;
-(void)setDefaultMinificationFilterName:(NSString *)arg1 ;
-(id)_wrapperViewForRequester:(id)arg1 ;
-(id)_hostViewForRequester:(id)arg1 enableAndOrderFront:(BOOL)arg2 ;
-(void)enableHostingForRequester:(id)arg1 priority:(long long)arg2 ;
-(id)_overrideRequesterIfNecessary:(id)arg1 ;
-(void)_updateActiveHostRequester;
-(void)disableHostingForRequester:(id)arg1 ;
-(void)_callOutToObservers:(/*^block*/id)arg1 ;
-(void)setLayer:(id)arg1 hidden:(BOOL)arg2 forRequester:(id)arg3 ;
-(id)_hostViewForRequester:(id)arg1 ;
-(id)snapshotContextForRequester:(id)arg1 ;
-(id)_snapshotContextForFrame:(CGRect)arg1 excludedContextIDs:(id)arg2 opaque:(BOOL)arg3 outTransform:(CGAffineTransform*)arg4 ;
-(id)snapshotViewWithContext:(id)arg1 ;
-(id)_activeHostRequester;
-(void)_activateRequester:(id)arg1 ;
-(void)enableHostingForRequester:(id)arg1 orderFront:(BOOL)arg2 ;
-(id)_snapshotContextForFrame:(CGRect)arg1 excludedLayers:(id)arg2 opaque:(BOOL)arg3 ;
-(id)initWithLayerManager:(id)arg1 scene:(id)arg2 ;
-(BOOL)defaultClippingDisabled;
-(CGAffineTransform)defaultHostViewTransform;
-(void)setDefaultBackgroundColorWhileHosting:(UIColor *)arg1 ;
-(UIColor *)defaultBackgroundColorWhileHosting;
-(void)setDefaultBackgroundColorWhileNotHosting:(UIColor *)arg1 ;
-(UIColor *)defaultBackgroundColorWhileNotHosting;
-(id)hostViewForRequester:(id)arg1 ;
-(id)hostViewForRequester:(id)arg1 appearanceStyle:(unsigned long long)arg2 ;
-(id)hostViewForRequester:(id)arg1 enableAndOrderFront:(BOOL)arg2 ;
-(id)hostViewForRequester:(id)arg1 enableAndOrderFront:(BOOL)arg2 appearanceStyle:(unsigned long long)arg3 ;
-(void)orderRequesterFront:(id)arg1 ;
-(void)setContextId:(unsigned)arg1 hidden:(BOOL)arg2 forRequester:(id)arg3 ;
-(void)setLayer:(id)arg1 alpha:(double)arg2 forRequester:(id)arg3 ;
-(id)disableHostingForReason:(id)arg1 ;
-(id)snapshotViewForSnapshot:(id)arg1 ;
-(id)snapshotViewWithFrame:(CGRect)arg1 excludingContexts:(id)arg2 opaque:(BOOL)arg3 ;
-(id)snapshotUIImageForFrame:(CGRect)arg1 excludingContexts:(id)arg2 opaque:(BOOL)arg3 outTransform:(CGAffineTransform*)arg4 ;
-(CGImageRef)snapshotCGImageRefForFrame:(CGRect)arg1 excludingContexts:(id)arg2 opaque:(BOOL)arg3 outTransform:(CGAffineTransform*)arg4 ;
-(void)_setContentState:(long long)arg1 ;
-(unsigned long long)defaultHostedLayerTypes;
-(unsigned long long)defaultRenderingMode;
-(NSString *)defaultMinificationFilterName;
-(long long)contentState;
-(id)succinctDescription;
-(id)descriptionWithMultilinePrefix:(id)arg1 ;
-(id)descriptionBuilderWithMultilinePrefix:(id)arg1 ;
-(id)succinctDescriptionBuilder;
-(void)dealloc;
-(NSString *)identifier;
-(NSString *)description;
-(void)removeObserver:(id)arg1 ;
-(void)setIdentifier:(NSString *)arg1 ;
-(void)invalidate;
-(void)addObserver:(id)arg1 ;
-(BOOL)isInvalidated;
@end

@interface UIWindow (Private)
-(BOOL)launchApplicationWithIdentifier:(id)arg1 suspended:(BOOL)arg2 ;
- (void)_setRotatableViewOrientation:(long long)arg1 updateStatusBar:(BOOL)arg2 duration:(double)arg3 force:(BOOL)arg4 ;
@end

@interface SBApplication : NSObject
@property(copy) NSString* displayIdentifier;
@property(copy) NSString* bundleIdentifier;
- (id)valueForKey:(id)arg1;
- (NSString *)displayName;
- (int)pid;
- (id)mainScene;
- (NSString *)path;
- (id)mainScreenContextHostManager;
- (void)setDeactivationSetting:(unsigned int)setting value:(id)value;
- (void)setDeactivationSetting:(unsigned int)setting flag:(BOOL)flag;
- (id)bundleIdentifier;
- (id)displayIdentifier;
- (void)notifyResignActiveForReason:(int)reason;
- (void)notifyResumeActiveForReason:(int)reason;
- (void)activate;
- (void)setFlag:(long long)arg1 forActivationSetting:(unsigned int)arg2;
- (BOOL)statusBarHidden;
@end
@interface SBIcon : NSObject
@property (nonatomic, retain) NSString* applicationBundleID;
-(NSString*)displayNameForLocation:(NSInteger)location;
-(UIImage*)generateIconImage:(int)arg1;
@end
@interface SBApplicationIcon : NSObject
@property (nonatomic, retain) SBApplication* application;
@end
@interface SBIconView : UIView
@property (nonatomic, retain) SBIcon* icon;
@end
@interface SBIconModel : NSObject
-(id)expectedIconForDisplayIdentifier:(NSString*)ident;
@end
@interface SBRootFolderController : NSObject
@property (nonatomic, retain) UIView* contentView;
@end
@interface SBIconController : NSObject
+(id)sharedInstance;
@property (nonatomic, retain) SBIconModel* model;
@property (nonatomic, assign) BOOL isEditing;
-(UIView*)currentRootIconList;
-(UIView*)dockListView;
-(SBRootFolderController*)_rootFolderController;
-(SBRootFolderController*)_currentFolderController;
-(void)clearHighlightedIcon;
-(NSInteger)currentIconListIndex;
-(void)removeIcon:(id)arg1 compactFolder:(BOOL)arg2;
-(id)insertIcon:(id)arg1 intoListView:(id)arg2 iconIndex:(long long)arg3 moveNow:(BOOL)arg4 pop:(BOOL)arg5;
-(id)rootFolder;
-(UIView*)iconListViewAtIndex:(NSInteger)index inFolder:(id)folder createIfNecessary:(BOOL)create;
-(BOOL)scrollToIconListAtIndex:(long long)arg1 animate:(BOOL)arg2;
-(NSArray*)allApplications;
-(BOOL)_canRevealShortcutMenu;
-(void)_revealMenuForIconView:(SBIconView*)icon presentImmediately:(BOOL)immediately;
-(void)_dismissShortcutMenuAnimated:(BOOL)animated completionHandler:(id)completionHandler;
@end
@interface SBApplicationShortcutMenuBackgroundView : UIView
@end
@class FBScene, FBWindowContextHostManager, FBSMutableSceneSettings;
// extern "C" void BKSHIDServicesCancelTouchesOnMainDisplay();

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

#define kScreenWidth [[UIScreen mainScreen] bounds].size.width
#define kScreenHeight [[UIScreen mainScreen] bounds].size.height

#ifdef __cplusplus
extern "C" {
#endif

CFNotificationCenterRef CFNotificationCenterGetDistributedCenter(void);

#ifdef __cplusplus
}
#endif

@interface FBScene
- (id)hostManager;
- (id)contextHostManager;
- (id)mutableSettings;
-(void)updateSettings:(id)arg1 withTransitionContext:(id)arg2;
-(void)updateSettings:(id)arg1 withTransitionContext:(id)arg2 completion:(id)arg3;
-(void)_applyMutableSettings:(id)arg1 withTransitionContext:(id)arg2 completion:(id)arg3;
@end

@interface SBUIController : NSObject
+(id)sharedInstance;
-(void)clickedMenuButton;
@end

@interface FBWindowContextHostManager : NSObject
- (void)enableHostingForRequester:(id)arg1 orderFront:(BOOL)arg2;
- (void)enableHostingForRequester:(id)arg1 priority:(int)arg2;
- (void)disableHostingForRequester:(id)arg1;
- (id)hostViewForRequester:(id)arg1 enableAndOrderFront:(BOOL)arg2;
@end

@interface FBWindowContextHostView : UIView
- (BOOL)isHosting;
@end

@interface FBSMutableSceneSettings
- (void)setBackgrounded:(bool)arg1;
@end

@interface SBApplicationController
+ (id)sharedInstance;
- (id)applicationWithBundleIdentifier:(NSString *)bid;
@end

@interface UIApplication (Private)
- (void)_relaunchSpringBoardNow;
- (id)_accessibilityFrontMostApplication;
- (void)launchApplicationWithIdentifier: (NSString*)identifier suspended: (BOOL)suspended;
- (id)displayIdentifier;
- (void)setStatusBarHidden:(bool)arg1 animated:(bool)arg2;
void receivedStatusBarChange(CFNotificationCenterRef center, void *observer, CFStringRef name, const void *object, CFDictionaryRef userInfo);
void receivedLandscapeRotate();
void receivedPortraitRotate();
@end

@interface SBBannerContextView : UIView
@end

@interface SBAppSwitcherModel : NSObject
+ (id)sharedInstance;
- (id)snapshotOfFlattenedArrayOfAppIdentifiersWhichIsOnlyTemporary;
@end

@interface SBAppSwitcherController : NSObject
- (id)_snapshotViewForDisplayItem:(id)arg1;
@end

@interface SBDisplayItem : NSObject
+ (id)displayItemWithType:(NSString *)arg1 displayIdentifier:(id)arg2;
@end

@interface SBAppSwitcherSnapshotView : NSObject
-(void)_loadSnapshotSync;
@end

@interface _UIBackdropViewSettings : NSObject
+(id)settingsForStyle:(NSInteger)style graphicsQuality:(NSInteger)quality;
+(id)settingsForStyle:(NSInteger)style;
-(void)setDefaultValues;
-(id)initWithDefaultValues;
@end
@interface _UIBackdropViewSettingsCombiner : _UIBackdropViewSettings
@end
@interface _UIBackdropView : UIView
-(id)initWithFrame:(CGRect)frame autosizesToFitSuperview:(BOOL)autoresizes settings:(_UIBackdropViewSettings*)settings;
@end

@interface SBAppToAppWorkspaceTransaction
- (void)begin;
- (id)initWithAlertManager:(id)alertManager exitedApp:(id)app;
- (id)initWithAlertManager:(id)arg1 from:(id)arg2 to:(id)arg3 withResult:(id)arg4;
- (id)initWithTransitionRequest:(id)arg1;
@end

@interface FBWorkspaceEvent : NSObject
+ (instancetype)eventWithName:(NSString *)label handler:(id)handler;
@end

@interface FBWorkspaceEventQueue : NSObject
+ (instancetype)sharedInstance;
- (void)executeOrAppendEvent:(FBWorkspaceEvent *)event;
@end
@interface SBDeactivationSettings
-(id)init;
-(void)setFlag:(int)flag forDeactivationSetting:(unsigned)deactivationSetting;
@end
@interface SBWorkspaceApplicationTransitionContext : NSObject
@property(nonatomic) _Bool animationDisabled; // @synthesize animationDisabled=_animationDisabled;
- (void)setEntity:(id)arg1 forLayoutRole:(int)arg2;
@end
@interface SBWorkspaceDeactivatingEntity
@property(nonatomic) long long layoutRole; // @synthesize layoutRole=_layoutRole;
+ (id)entity;
@end
@interface SBWorkspaceHomeScreenEntity : NSObject
@end
@interface SBMainWorkspaceTransitionRequest : NSObject
- (id)initWithDisplay:(id)arg1;
@end

static int const UITapticEngineFeedbackPeek = 1001;
static int const UITapticEngineFeedbackPop = 1002;
@interface UITapticEngine : NSObject
- (void)actuateFeedback:(int)arg1;
- (void)endUsingFeedback:(int)arg1;
- (void)prepareUsingFeedback:(int)arg1;
@end
@interface UIDevice (Private)
-(UITapticEngine*)_tapticEngine;
@end

OBJC_EXTERN UIImage* _UICreateScreenUIImage(void) NS_RETURNS_RETAINED;
