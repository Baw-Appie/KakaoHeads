#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface FCViewController : UIViewController

@property (nonatomic, strong) NSString *chatID;
@property (nonatomic, strong) UIView *AppView;

+ (instancetype)sharedInstance;
- (id)init;
- (void)viewDidLoad;
- (void)KakaoHandler:(NSString *)name withUserInfo:(NSDictionary *)userinfo;
@end
