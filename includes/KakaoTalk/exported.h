@interface Chat
{
    // UIImage *thumbnailImage_;
    // bool isDownloadingThumbnailImage;
    // UIImage *largeThumbnailImage_;
    // bool isDownloadingLargeThumbnailImage;
    // NSDictionary *_cachedExtraInfo;
    // NSDictionary *userInfoCache;
    // NSDictionary *extraInfoCache;
    // NSString *decryptedLastMessage;
    // NSString *oldThubnailImageUrl;
    // NSString *oldLargeThubnailImageUrl;
    // bool shouldLeave;
    // bool hasErrorMessages;
    // bool _needToReloadMessages;
    // ChatMessage *firstMessage;
    // NSObject<ChatMessageDelegate> *messageDelegate;
    // UIImage *_localImageForCreateGroupChatRoom;
    // NSNumber *candidateLastSeenMessageID;
    // ChatMessage *_lastEmoticonMessage;
    // NSString *nickNameForCreateGroupChatRoom;
    // NSString *profileImageUrlForCreateGroupChatRoom;
}
+ (id)randomHostImage;
+ (id)chatWithBackupDictionary:(id)v1;
+ (id)chatWithBackupDictionary_v2:(id)v1;
+ (id)chatDefaultSortDescriptorsByDate;
+ (id)chatDefaultSortDescriptorsByFavorite;
+ (id)chatDefaultSortDescriptorsByUnreadCount;
+ (id)chatDefaultSortDescriptors;
+ (id)chatDefaultSortDescriptorsByPinIndex;
+ (void)loginListWithRequest:(id)v1 completion:(void (^/* unknown block signature */)(void))v2;
+ (void)processChatDatas:(id)v1 lastLinkToken:(int)v2 completion:(void (^/* unknown block signature */)(void))v3;
+ (void)updateChatDatasAndUnknownLinkIds:(id)v1 chatDatas:(id)v2 completion:(void (^/* unknown block signature */)(void))v3;
+ (void)lChatListWithRequest:(id)v1 completion:(void (^/* unknown block signature */)(void))v2;
+ (void)getMetasWithRequest:(id)v1 completion:(void (^/* unknown block signature */)(void))v2;
+ (void)mChatLogsWithRequest:(id)v1 completion:(void (^/* unknown block signature */)(void))v2;
+ (unsigned int)getLatestPKWithMemberIds:(id)v1 isMulti:(bool)v2 completion:(void (^/* unknown block signature */)(void))v3;
+ (void)decryptChatLogsOfChats:(id)v1 completion:(void (^/* unknown block signature */)(void))v2;
+ (unsigned int)getMCMetaWithCompletion:(void (^/* unknown block signature */)(void))v1;
+ (unsigned int)chatInfoWithChatId:(long long)v1 completion:(void (^/* unknown block signature */)(void))v2;
+ (unsigned int)chatInfoWithChatId:(long long)v1 type:(id)v2 completion:(void (^/* unknown block signature */)(void))v3;
+ (unsigned int)chatInfoWithChatId:(long long)v1 creatorId:(long long)v2 type:(id)v3 completion:(void (^/* unknown block signature */)(void))v4;
+ (unsigned int)getTokenWithTypes:(id)v1 completion:(void (^/* unknown block signature */)(void))v2;
+ (void)_processUpdateChatInfoResponse:(id)v1 creatorId:(long long)v2 error:(id)v3 completion:(void (^/* unknown block signature */)(void))v4;
+ (void)notiReceivesWithCompletion:(void (^/* unknown block signature */)(void))v1;
+ (id)createChatWith:(id)v1 withChatType:(long long)v2 withMembers:(id)v3;
+ (id)chatFromID:(id)v1 withChatType:(long long)v2 withMember:(id)v3;
+ (id)insertNewChatWithEntityName:(id)v1 viewType:(long long)v2;
+ (id)insertNewChatWithUser:(id)v1 viewType:(long long)v2;
+ (id)insertNewChatWithUsers:(id)v1 viewType:(long long)v2;
+ (id)insertNewChatWithUsers:(id)v1 entityName:(id)v2 viewType:(long long)v3;
+ (id)insertNewChatWithEntityName:(id)v1 viewType:(long long)v2 context:(id)v3;
+ (id)insertNewChatWithUser:(id)v1 viewType:(long long)v2 context:(id)v3;
+ (id)insertNewChatWithUsers:(id)v1 viewType:(long long)v2 context:(id)v3;
+ (id)insertNewChatWithUsers:(id)v1 entityName:(id)v2 viewType:(long long)v3 context:(id)v4;
+ (id)predicateForAllChats;
+ (id)fetchAll;
+ (id)fetchByID:(id)v1;
+ (id)fetchByID:(id)v1 entityName:(id)v2;
+ (id)safeFetchByID:(id)v1;
+ (id)fetchByUser:(id)v1;
+ (id)fetchAllWithContext:(id)v1;
+ (id)fetchAllWithEntityName:(id)v1;
+ (id)fetchByID:(id)v1 context:(id)v2;
+ (id)fetchByID:(id)v1 entityName:(id)v2 context:(id)v3;
+ (id)fetchByUser:(id)v1 context:(id)v2;
+ (id)predicateForPinnedChat;
+ (id)fetchPinnedChats:(id)v1;
+ (id)insertNewOrFetchByUser:(id)v1;
+ (id)insertNewOrFetchByUsers:(id)v1;
+ (id)insertNewOrFetchByUser:(id)v1 context:(id)v2;
+ (id)insertNewOrFetchByUsers:(id)v1 context:(id)v2;
+ (id)insertNewOrFetchByUser:(id)v1 andChatId:(id)v2;
+ (id)insertNewOrFetchByUsers:(id)v1 andChatId:(id)v2;
+ (id)insertNewOrFetchByUser:(id)v1 andChatId:(id)v2 context:(id)v3;
+ (id)insertNewOrFetchByUsers:(id)v1 andChatId:(id)v2 context:(id)v3;
+ (void)updateWithLChatListChatDatas:(id)v1;
+ (void)updateWithLChatListChatDatasForSyncUnreadCount:(id)v1;
+ (void)updateWithMChatLogsParameter:(id)v1;
+ (void)updateWithGetMetasResponse:(id)v1;
+ (void)addChatLogs:(id)v1 bookmark:(bool)v2;
+ (void)updateWithLocoChatInfoObject:(id)v1 completion:(void (^/* unknown block signature */)(void))v2;
+ (void)updateWithLocoChatInfoObject:(id)v1 creatorId:(long long)v2 completion:(void (^/* unknown block signature */)(void))v3;
+ (void)_processChatInfoObject:(id)v1 creatorId:(long long)v2 onChat:(id)v3 openLink:(id)v4;
+ (void)updateWithGetLPKResponse:(id)v1;
+ (void)createChatRoomByChatRoomObject:(id)v1 openLinkObject:(id)v2;
+ (void)updateWithCreateLinkResponse:(id)v1;
+ (void)updateWithSyncLinkCreatedResponse:(id)v1;
+ (void)updateWithLocoChatDatas:(id)v1;
+ (void)updateWithLocoChatDatasForSyncUnreadCount:(id)v1;
+ (bool)updatePinWithChat:(id)v1 isPin:(bool)v2;
+ (void)leaveAndDeleteWithChatIds:(id)v1;
+ (void)kickedWithChatIds:(id)v1;
+ (long long)minLogIdFromChatData:(id)v1;
+ (void)checkUnreadMesageWithLocoChatDataObject:(id)v1 chat:(id)v2;
+ (void)checkUnreadMesageWithLocoChatDatas:(id)v1;
+ (id)entityNameForServerChatType:(id)v1;
+ (id)sharedCache;
+ (void)resetAllCaches;
+ (void)resetCaches;
+ (void)mergeMemberAfterChatRestore;
+ (void)mergeDuplicateChatsWithContext:(id)v1;
+ (id)mergedChats:(id)v1;
+ (long long)chatTypeFromServerChatType:(id)v1;
+ (id)entityName;
+ (bool)isSecretChatWithChatId:(long long)v1;
+ (bool)isNormalChatWithChatId:(long long)v1;
+ (bool)isPlusChatWithChatId:(long long)v1;
+ (bool)isOpenChatWithChatId:(long long)v1;
+ (bool)isPlusDailyCardWithChatId:(long long)v1;
+ (id)lastServerLogIdWithChatId:(id)v1 userId:(id)v2;
+ (id)lastServerLogIdWithChatId:(id)v1;
+ (id)reportChatMessagesWithChatId:(id)v1;
+ (id)reportChatMessagesWithChatId:(id)v1 logId:(id)v2;
- (void)carryOnWriteWithCompletion:(void (^/* unknown block signature */)(void))v1;
- (void)carryOnStopWithCompletion:(void (^/* unknown block signature */)(void))v1;
- (id)defaultHostImage;
- (void)enableCacheIsolation;
- (bool)shouldMigrateMediaCache;
- (id)cachePathForIsolation;
- (id)cachePathForType:(long long)v1;
- (id)secretGroupChatProfilePathWithFileName:(id)v1;
- (id)generageSecretGroupChatProfileFileName;
- (id)generageSecretGroupChatLargeProfileFileName;
- (id)todayMessages;
- (void)enumerateCacheFilesUsingBlock:(void (^/* unknown block signature */)(void))v1;
- (void)deleteAllCaches;
- (bool)isBlindMemberWithUserID:(id)v1;
- (id)chatMetaChatBackgroundImageUrlString;
- (id)plusChatTvUrlString;
- (long long)plusChatTvMetaRevision;
- (bool)isPlusChatTVOnLive;
- (long long)numberOfLiveTalkMembers;
- (id)liveTalkMeta;
- (id)watchTVTogetherActionUser;
- (id)openChatTvUrlString;
- (long long)openChatTvMetaRevision;
- (bool)isNoticePinned;
- (id)miniProfile_profileImageUrl;
- (id)miniProfile_fullProfileImageUrl;
- (id)chatRoomTypeforBackup;
- (id)dictionaryForBackup_v2;
- (unsigned long long)chatLogsCountFromChatLogID:(long long)v1;
- (unsigned long long)chatLogsCountFromLastSyncID:(long long)v1 betweenNextSyncID:(long long)v2;
- (unsigned long long)chatLogsCountExceptFeedTypeFromLastSyncID:(long long)v1 betweenNextSyncID:(long long)v2;
- (unsigned long long)chatLogsCountFromLastReadID:(long long)v1 toMinLogId:(long long)v2;
- (unsigned long long)chatLogsCountFromLastSyncID:(long long)v1 outNextSyncID:(long long *)v2;
- (unsigned long long)chatLogsCountFromChatLogID:(long long)v1 excludeFeedMessage:(bool)v2;
- (unsigned int)getStatusWithCompletion:(void (^/* unknown block signature */)(void))v1;
- (unsigned int)setStatus:(id)v1 WithCompletion:(void (^/* unknown block signature */)(void))v2;
- (unsigned int)chatOnRoomWithCompletion:(void (^/* unknown block signature */)(void))v1;
- (unsigned int)chatOffWithCompletion:(void (^/* unknown block signature */)(void))v1;
- (unsigned int)getMetaWithMetaRevisions:(id)v1 completion:(void (^/* unknown block signature */)(void))v2;
- (unsigned int)getMoimMetaWithMetaRevisions:(id)v1 completion:(void (^/* unknown block signature */)(void))v2;
- (unsigned int)moClickWithMoimMeta:(id)v1 completion:(void (^/* unknown block signature */)(void))v2;
- (unsigned int)syncBadgeWithCompletion:(void (^/* unknown block signature */)(void))v1;
- (unsigned int)syncBadgeWithNextLogId:(long long)v1 currentLogId:(long long)v2 completion:(void (^/* unknown block signature */)(void))v3;
- (unsigned int)syncMessageByForce:(bool)v1 completion:(void (^/* unknown block signature */)(void))v2;
- (unsigned int)syncMessageWithNextLogId:(long long)v1 currentLogId:(long long)v2 completion:(void (^/* unknown block signature */)(void))v3;
- (unsigned int)memberWithMemberIds:(id)v1 completion:(void (^/* unknown block signature */)(void))v2;
- (unsigned int)leaveWithCompletion:(void (^/* unknown block signature */)(void))v1;
- (unsigned int)leaveAndBlocked:(bool)v1 completion:(void (^/* unknown block signature */)(void))v2;
- (unsigned int)createOrPCreateWithMemberIds:(id)v1 completion:(void (^/* unknown block signature */)(void))v2;
- (unsigned int)addMemWithMemberIds:(id)v1 completion:(void (^/* unknown block signature */)(void))v2;
- (unsigned int)updateChatMetaType:(int)v1 content:(id)v2 completion:(void (^/* unknown block signature */)(void))v3;
- (unsigned int)updateNotice:(id)v1 completion:(void (^/* unknown block signature */)(void))v2;
- (unsigned int)updateKakaoTvURL:(id)v1 completion:(void (^/* unknown block signature */)(void))v2;
- (unsigned int)updateCloseLiveTalkWithCompletion:(void (^/* unknown block signature */)(void))v1;
- (unsigned int)updateNoticePin:(bool)v1 completion:(void (^/* unknown block signature */)(void))v2;
- (unsigned int)updatePushAlert:(bool)v1 completion:(void (^/* unknown block signature */)(void))v2;
- (unsigned int)updateChatName:(id)v1 completion:(void (^/* unknown block signature */)(void))v2;
- (unsigned int)updateChatProfileImage:(id)v1 completion:(void (^/* unknown block signature */)(void))v2;
- (unsigned int)updateChatFavorite:(bool)v1 completion:(void (^/* unknown block signature */)(void))v2;
- (unsigned int)updateChatPushAlertSoundName:(id)v1 completion:(void (^/* unknown block signature */)(void))v2;
- (unsigned int)mChatLogsWithSinceId:(long long)v1 completion:(void (^/* unknown block signature */)(void))v2;
- (void)checkMsgWrittenViaMChatLogsSinceId:(long long)v1 msgId:(long long)v2 completion:(void (^/* unknown block signature */)(void))v3;
- (unsigned int)updateNewSecretKey:(id)v1 completion:(void (^/* unknown block signature */)(void))v2;
- (unsigned int)sCreateWithMemberIds:(id)v1 completion:(void (^/* unknown block signature */)(void))v2;
- (unsigned int)getSecretKeyWithSkeyToken:(long long)v1 completion:(void (^/* unknown block signature */)(void))v2;
- (unsigned int)getLatestPKWithCompletion:(void (^/* unknown block signature */)(void))v1;
- (unsigned int)sAddMemWithMemberIds:(id)v1 completion:(void (^/* unknown block signature */)(void))v2;
- (unsigned int)deleteMessage:(id)v1 completion:(void (^/* unknown block signature */)(void))v2;
- (unsigned int)kickMemWithUserId:(long long)v1 completion:(void (^/* unknown block signature */)(void))v2;
- (void)markRead;
- (void)postDidChatMessagesContentsUpdated:(id)v1;
- (void)postDidChatMessageUpdate:(id)v1;
- (void)postDidChatMessagesAdded:(id)v1 rewrittenMessages:(id)v2 bookmark:(bool)v3;
- (void)postDidLostMarkAdded:(id)v1;
- (void)postDidChangeAccountMarkAdded:(id)v1;
- (void)updateWithChatRoomObject:(id)v1;
- (id)fetchByUserId:(id)v1;
- (void)prepareForDeletion;
- (void)removeAllMessages;
- (void)deleteChat;
- (void)deleteNormalFiles;
- (void)deleteChatWithContext:(id)v1;
- (id)updateWithCreateResponse:(id)v1 memberIds:(id)v2;
- (void)updateWithChatOnRoomResponse:(id)v1;
- (void)updateWithSyncMessageResponse:(id)v1;
- (void)updateWithSyncBadgeResponse:(id)v1;
- (void)updateWithMsgResponse:(id)v1;
- (void)updateWithDecunreadResponses:(id)v1;
- (void)updateWithLeftResponse:(id)v1;
- (void)updateWithHintResponse:(id)v1;
- (void)updateWithNewMemResponse:(id)v1;
- (id)updateMember:(id)v1;
- (void)updateWithLocoChatRoomObject:(id)v1;
- (void)updateWithLocoInviteInfoObjects:(id)v1;
- (void)updateWithLocoChatDataObject:(id)v1 exceptUnreadCount:(bool)v2;
- (void)updateWithLastLocoChatLogObject:(id)v1 joinedAt:(long long)v2;
- (void)updateWithLastLocoChatLogObject:(id)v1;
- (void)_updateWithLocoChatInfoObject:(id)v1 openLink:(id)v2;
- (void)updateWithLocoChatLogObject:(id)v1 message:(id)v2;
- (void)updateWithLocoChatLogFeedInvitationObject:(id)v1 feedLogId:(long long)v2;
- (void)updateWithDelMemResponse:(id)v1;
- (void)updateWithKickMemberResponse:(id)v1;
- (void)updateWithKickedResponse:(id)v1;
- (void)updateWithLinkKickedResponse:(id)v1;
- (void)updateWithLinkDeletedResponse:(id)v1;
- (void)updateWithReportMemberResponse:(id)v1;
- (void)updateWithLocoChatLogFeedLeaveObject:(id)v1 feedLogId:(long long)v2;
- (void)updateWithLocoChatLogFeedDeleteObject:(id)v1 feedLogId:(long long)v2;
- (void)updateWithLocoChatLogJoinObject:(id)v1 feedLogId:(long long)v2;
- (void)updateWithReportLeaveResponse:(id)v1;
- (void)updateWithKickLeaveResponse:(id)v1;
- (void)updateWithLocoChatLogFeedKickObject:(id)v1 feedLogId:(long long)v2 isReported:(bool)v3;
- (void)updateWithLocoChatMetaObject:(id)v1;
- (void)updateWithLocoMoimMetaObject:(id)v1;
- (void)updateMinimizedStatusForLiveTalkNotice:(bool)v1;
- (void)updateWithLocoMoimMetaObjects:(id)v1;
- (void)updateChatMetaWith:(id)v1;
- (id)updateReadmarks:(id)v1 withMemberIDs:(id)v2;
- (id)readmarkDictionary:(id)v1 memberIds:(id)v2;
- (void)updateReadmarks:(id)v1 withMemberIDsUpdated:(id)v2;
- (void)addActiveMemberIds:(id)v1 readMarks:(id)v2;
- (void)addActiveMemberId:(id)v1 readMark:(id)v2;
- (void)removeActiveMemberId:(id)v1;
- (void)removeAllActiveMembers;
- (void)updateWithSWriteResponse:(id)v1;
- (void)updateWithSAddMemResponse:(id)v1;
- (void)updateWithSetSKResponse:(id)v1;
- (void)updateWithSCreateResponse:(id)v1;
- (void)reconstructMembersIfInvalidMemberExistsWithUserId:(long long)v1;
- (void)updateWithJoinLinkResponse:(id)v1;
- (void)updateOpenChatWithLinkId:(long long)v1;
- (void)addBlindMemberId:(id)v1;
- (bool)isBlindChatLogObject:(id)v1;
- (id)findLastUserChatLogWithAddedNewChatLogs:(id)v1;
- (void)updateLastMessageChatLog:(id)v1;
- (void)updateLastRewrittenMessageChatLog:(id)v1;
- (id)updateRewriteMessages:(id)v1;
- (id)insertMessagesFromChatLogObjects:(id)v1;
- (void)addMessageWithLocoChatLogObjects:(id)v1 bookmark:(bool)v2;
- (void)updateScrapUrlWithTextMessages:(id)v1;
- (void)updateScrapDataWithTextMessages:(id)v1;
- (void)downloadTrailerWithSingleMediaMessage:(id)v1 chatId:(long long)v2;
- (void)downloadTrailerWithCollageMessage:(id)v1 chatId:(long long)v2;
- (void)clearInvitees;
- (bool)shouldAlarmV2:(id)v1;
- (bool)hasReceivedUserMessage;
- (void)removeCache;
- (bool)existSendingMsgClientMsgID:(id)v1;
- (bool)removeSendingMsgClientMsgID:(id)v1;
- (void)removeDupMessage:(id)v1;
- (void)insertChangeAccountMarkRecord:(id)v1 withChatId:(id)v2;
- (void)updateChangeAccountStatusWithChatLogs:(id)v1;
- (void)decreaseUnreadCountWithLogId:(id)v1;
- (void)updateWithLastMessageDelete;
- (id)defaultChatDictionary;
- (id)newMessageHintForWatch;
- (id)readHintForWatch;
- (id)updateHintWithType:(id)v1;
- (id)dataForWatch;
- (id)thumbailForWatch;
- (id)defaultImageOfUser:(id)v1;
- (id)makeThumbnail;
- (void)awakeFromInsert;
- (id)parsedExtraInfo;
- (void)updateParsedExtraInfo:(id)v1;
- (void)setParsedExtraInfo:(id)v1;
- (bool)isTemporary;
- (bool)validChatID;
- (bool)isSingleChat;
- (bool)isGroupChat;
- (bool)isPlusChat;
- (bool)isPlusChatsFolder;
- (bool)isKindOfNormalChat;
- (bool)isKindOfSecretChat;
- (bool)isSecretSingleChat;
- (bool)isSecretGroupChat;
- (bool)isKindOfSingleChat;
- (bool)isKindOfGroupChat;
- (bool)isOpenSingleChat;
- (bool)isOpenGroupChat;
- (bool)isOpenChatsFolder;
- (bool)isKindOfOpenChat;
- (bool)isMemoChat;
- (bool)isDailyCardChat;
- (bool)availableBoard;
- (id)mainMember;
- (id)mainMembersWithMaxCount:(unsigned long long)v1;
- (id)parsedUserInfo;
- (void)setParsedUserInfo:(id)v1;
- (void)setRooomName:(id)v1;
- (id)defaultTitle;
- (id)displayTitle;
- (id)displayUnreadCount;
- (id)displayTitleWithLength:(long long)v1;
- (id)displayTitleInChatRoom;
- (id)metaInfoForOpenChatClientLog;
- (id)metaInfoForClientLog;
- (id)metaInfoValueForClientLog;
- (id)firstReceivedMessage;
- (id)firstMessageUserId;
- (bool)findChatRoomWithTargetText:(id)v1 searchText:(id)v2;
- (bool)isSearchableWithText:(id)v1;
- (bool)isSearchableMembersWithText:(id)v1;
- (bool)isSearchableWithMembers:(id)v1 keyword:(id)v2;
- (void)setlocalImageForCreateGroupChatRoom:(id)v1;
- (bool)isInvalid;
- (bool)isBlockedSingleChat;
- (bool)isDeactivatedSingleChat;
- (id)moimMeta;
- (void)setDirectChatID:(id)v1;
- (bool)isWritableChatOnPickingMember;
- (bool)canPushAlertWithMessage:(id)v1;
- (bool)canUpdateBadgeWithMessage:(id)v1;
- (void)chatRoomFileCache:(id)v1 didDownloadWithData:(id)v2;
- (void)chatRoomFileCacheDidFailToDownload:(id)v1;
- (bool)isHiddenChat;
@end

@interface AppNavigationController
-(void)moveToChatRoomWithChat:(void *)arg2 withParameters:(void *)arg3 isFirst:(bool)arg4 forceNewChat:(bool)arg5;
@end
