#import <AppSupport/CPDistributedMessagingCenter.h>
#import <rocketbootstrap/rocketbootstrap.h>
#import <KakaoTalk/exported.h>

int licensed;
static CPDistributedMessagingCenter *center;
extern "C" CFPropertyListRef MGCopyAnswer(CFStringRef property);

%hook TalkAppDelegate
-(BOOL)application:(id)arg1 didFinishLaunchingWithOptions:(id)arg2 {
  HBLogDebug(@"KakaoTalk is Started!");
  bool o =  %orig;

  center = [CPDistributedMessagingCenter centerNamed:@"com.c1d3r.messagehub"];
  rocketbootstrap_distributedmessagingcenter_apply(center);

  [center sendMessageName:@"debug" userInfo:@{@"message":[NSString stringWithFormat:@"%@ launched and hooked!", @"com.iwilab.KakaoTalk"]}];
  [center sendMessageName:@"registerExtension" userInfo:@{@"bundleId" : @"com.iwilab.KakaoTalk"}];

  [[objc_getClass("NSDistributedNotificationCenter") defaultCenter] addObserverForName:[NSString stringWithFormat:@"com.c1d3r.messagehub.%@.openConversation", @"com.iwilab.KakaoTalk"] object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *notification) {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *chatID = [notification.userInfo objectForKey:@"conversationId"];
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber *cid = [f numberFromString:chatID];
        id chat = [objc_getClass("Chat") fetchByID:cid];

        [objc_getClass("Chat") openChatRoomWithChat:chat];
        [[objc_getClass("AppNavigationController") defaultController] openChatting:chat];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
          [objc_getClass("Chat") openChatRoomWithChat:chat];
          [[objc_getClass("AppNavigationController") defaultController] openChatting:chat];
        });
    });
  }];

  return o;
}
%end

void processData(NSString* package, id chatid) {
  HBLogDebug(@"Checking Package %@", package);
  if([package isEqualToString:@"com.iwilab.KakaoTalk"]) {
    NSNumber *chatID = (NSNumber *)chatid;
    UIImage *profileimg = [[objc_getClass("Chat") fetchByID:chatID] thumbailForWatch];
    // id unreadCount = [[objc_getClass("Chat") fetchByID:chatID] displayUnreadCount];
    id title = [[objc_getClass("Chat") fetchByID:chatID] displayTitle];
    HBLogDebug(@"requestChatHead.... %@", chatID);

    [center sendMessageName:@"messageReceived" userInfo:@{
      @"conversationId" : [NSString stringWithFormat:@"%@", chatID],
      @"recipients" : @[
                         @{
                           @"name": title,
                           @"id": title,
                           @"imageData": UIImagePNGRepresentation(profileimg)
                         }
                       ],
      @"message": @"",
      @"bundleId": @"com.iwilab.KakaoTalk"
    }];
  }
}

void notificationReceived(NSString* application, id chatid) {
  if(licensed == 0) {
    if (![[NSFileManager defaultManager] fileExistsAtPath:@"/var/lib/dpkg/info/com.rpgfarm.kakaoheads.list"]){
      licensed = 2;
      [[objc_getClass("NSDistributedNotificationCenter") defaultCenter] postNotificationName:@"com.rpgfarm.kakaoheadsplus.showalert" object:nil userInfo:nil];
    } else {
      NSString *udid = (__bridge NSString *)MGCopyAnswer(CFSTR("UniqueDeviceID"));
      HBLogDebug(@"Getting UDID, %@", udid);
      NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
      [request setHTTPMethod:@"GET"];
      [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://license.rpgfarm.com/checkTweakLicense?package=com.rpgfarm.kakaoheads&udid=%@", udid]]];
      [request setTimeoutInterval:3.0];

      NSHTTPURLResponse *responseCode = nil;
      NSError *error;
      NSData *oResponseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&responseCode error:&error];
      NSString *res = [[NSString alloc] initWithData:oResponseData encoding:NSUTF8StringEncoding];
      if(([responseCode statusCode] != 200 || ![res isEqualToString:@"1"]) && error == nil){
        licensed = 2;
        [[objc_getClass("NSDistributedNotificationCenter") defaultCenter] postNotificationName:@"com.rpgfarm.kakaoheadsplus.showalert" object:nil userInfo:nil];
      } else {
        licensed = 1;
        processData(application, chatid);
      }
    }
  }
  if(licensed == 1) processData(application, chatid);
}

%hook LocoPushNotification
+(void)showWithMessage:(void *)arg2 chatId:(unsigned long long)arg3 logId:(unsigned long long)arg4 soundName:(void *)arg5 userInfo:(void *)arg6 category:(void *)arg7 {
  %orig;
  notificationReceived(@"com.iwilab.KakaoTalk", [NSNumber numberWithUnsignedLongLong:arg3]);
}
%end


%ctor {
  if([[NSFileManager defaultManager] fileExistsAtPath:@"/Library/MobileSubstrate/DynamicLibraries/MessageHeadsXI.dylib"]) {
    %init();
    HBLogDebug(@"KakaoHeads by Baw Appie Started with ChatHeads compatibility mode");
  }
}
