#import <FCChatHeads/FCChatHeads.h>
#import <KakaoTalk/exported.h>
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <objc/runtime.h>
#import "FCViewController.h"
#import "FCWindow.h"
#import "CDTContextHostProvider.h"
#import <sys/stat.h>
#import <dlfcn.h>
#import <rocketbootstrap/rocketbootstrap.h>
#import <AppSupport/CPDistributedMessagingCenter.h>

@interface FCViewController () <FCChatHeadsControllerDatasource>
@end

@implementation FCViewController

-(id)init {
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
	[ChatHeadsController setHeadSuperView:self.view];
	[ChatHeadsController setDatasource:self];
	[ChatHeadsController setDelegate:self];
  HBLogDebug(@"Setup Observer.... %@", [ChatHeadsController headSuperView]);
	CPDistributedMessagingCenter * c = [CPDistributedMessagingCenter centerNamed:@"com.rpgfarm.kakaoheadsplus"];
  rocketbootstrap_distributedmessagingcenter_apply(c);
	[c runServerOnCurrentThread];
	[c registerForMessageName:@"receiver" target:self selector:@selector(KakaoHandler:withUserInfo:)];
	[c registerForMessageName:@"opener" target:self selector:@selector(ThreadOpener:withUserInfo:)];
  HBLogDebug(@"Complete Setup Observer....");
}

+ (instancetype)sharedInstance {
  static dispatch_once_t p = 0;
  __strong static id _sharedSelf = nil;
  dispatch_once(&p, ^{
      _sharedSelf = [[self alloc] init];
  });
  return _sharedSelf;
}

- (void)KakaoHandler:(NSString *)name withUserInfo:(NSDictionary *)userinfo {
  HBLogDebug(@"Response Observer");
  UIImage *profileimg = [UIImage imageWithData:[userinfo objectForKey:@"profileimg"]];
  NSString *chatID = [NSString stringWithFormat:@"%@", [userinfo objectForKey:@"chatID"]];
  NSString *chatTitle = (NSString *)[userinfo objectForKey:@"title"];
  NSString *package = [NSString stringWithFormat:@"%@", [userinfo objectForKey:@"package"]];
	[ChatHeadsController presentChatHeadWithImage:profileimg chatID:chatID];
  NSNumberFormatter *formatter=[[NSNumberFormatter alloc]init];
  [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
  NSNumber *numberObj = [formatter numberFromString:[NSString stringWithFormat:@"%@", [userinfo objectForKey:@"unreadCount"]]];
  [ChatHeadsController setUnreadCount:[numberObj integerValue] forChatHeadWithChatID:chatID];
  [ChatHeadsController setPackage:package forChatHeadWithChatID:chatID];
  [ChatHeadsController setProfileImage:profileimg forChatHeadWithChatID:chatID];
  [ChatHeadsController setChatTitle:chatTitle forChatHeadWithChatID:chatID];
	HBLogDebug(@"%@", profileimg);
	HBLogDebug(@"%@", chatID);
}

- (void)ThreadOpener:(NSString *)name withUserInfo:(NSDictionary *)userinfo {
  HBLogDebug(@"ThreadOpener Observer");
  NSString *package = [userinfo objectForKey:@"package"];
  if(self.chatID != nil) {
    if([package isEqualToString:@"com.iwilab.KakaoTalk"]) {
      NSDictionary *userInfo = @{ @"chatID": self.chatID };
      dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[objc_getClass("NSDistributedNotificationCenter") defaultCenter] postNotificationName:@"com.rpgfarm.kakaoheadsplus.kakaotalk.opener" object:nil userInfo:userInfo];
      });
    }
    self.chatID = nil;
  }
}

-(void)chatHeadsController:(FCChatHeadsController *)chatHeadsController willPresentPopoverForChatID:(NSString *)chatID {
  [ChatHeadsController setUnreadCount:0 forChatHeadWithChatID:chatID];
  self.chatID = chatID;
  NSDictionary *userInfo = @{ @"chatID": self.chatID };
  if([[chatHeadsController package:chatID] isEqualToString:@"com.iwilab.KakaoTalk"]) {
    [[objc_getClass("NSDistributedNotificationCenter") defaultCenter] postNotificationName:@"com.rpgfarm.kakaoheadsplus.kakaotalk.opener" object:nil userInfo:userInfo];
  }
}

-(void)chatHeadsController:(FCChatHeadsController *)chatHeadsController willDismissPopoverForChatID:(NSString *)chatID {
  NSString* openApplication = [chatHeadsController package:chatID];
  SBApplication *frontApp = [[UIApplication sharedApplication] _accessibilityFrontMostApplication];
  NSString *currentAppDisplayID = [frontApp displayIdentifier];
  if(![openApplication isEqualToString:currentAppDisplayID]) {
    [CDTContextHostProvider dehost:openApplication];
  }
  self.chatID = nil;
}

- (UIView *)chatHeadsController:(FCChatHeadsController *)chatHeadsController viewForPopoverForChatHeadWithChatID:(NSString *)chatID {
  HBLogDebug(@"loadData...");
  HBLogDebug(@"Package name is... %@", [chatHeadsController package:chatID]);
  HBLogDebug(@"Package name is... %@", [chatHeadsController profileImage:chatID]);
  HBLogDebug(@"Package name is... %@", [chatHeadsController chatTitle:chatID]);
  UIView *view = [[UIView alloc] initWithFrame:self.view.bounds];

  CGRect frameRect = self.view.bounds;
  frameRect.size.height = 50;
  UIView *TopFrame = [[UIView alloc] initWithFrame:frameRect];
  TopFrame.backgroundColor = [UIColor whiteColor];
  UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:TopFrame.bounds];
  [TopFrame.layer setMasksToBounds:NO];
  [TopFrame.layer setShadowColor:[[UIColor blackColor] CGColor]];
  [TopFrame.layer setShadowOffset:CGSizeMake(0.0f, 0.0f)];
  [TopFrame.layer setShadowOpacity:1.0f];
  [TopFrame.layer setShadowRadius:3.5f];
  [TopFrame.layer setShadowPath:shadowPath.CGPath];
  UILabel *myLabel = [[UILabel alloc] initWithFrame:TopFrame.bounds];
  NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
  attachment.image = [self imageWithRoundedCornersSize:40 usingImage:[chatHeadsController profileImage:chatID]];
  attachment.bounds = CGRectMake(10, -12, 40, 40);
  NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
  NSMutableAttributedString *myString= [[NSMutableAttributedString alloc] init];
  [myString appendAttributedString:attachmentString];
  [myString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"  %@", [chatHeadsController chatTitle:chatID]]]];
  myLabel.attributedText = myString;
  myLabel.font = [UIFont systemFontOfSize:17.0];
  myLabel.numberOfLines = 0;
  myLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
  myLabel.textColor = [UIColor colorWithWhite:0.2 alpha:1.0];
  myLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
  myLabel.adjustsFontSizeToFitWidth = YES;
  CGRect frame = myLabel.frame;
  frame.origin.x = 10.0;
  myLabel.frame = frame;
  [TopFrame addSubview:myLabel];

  UIImageView *ChatButton = [[UIImageView alloc] init];
  ChatButton.frame = CGRectMake(TopFrame.bounds.size.width-105, 0, 50, 50);
  ChatButton.image = [UIImage imageNamed:@"/Library/BawAppie/KakaoHeads/kakaotalk"];
  UITapGestureRecognizer *ChatTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ChatButtonTap)];
  ChatTap.numberOfTapsRequired = 1;
  [ChatButton setUserInteractionEnabled:YES];
  [ChatButton addGestureRecognizer:ChatTap];
  [TopFrame addSubview:ChatButton];
  UIImageView *voiceTalkImageView = [[UIImageView alloc] init];
  voiceTalkImageView.frame = CGRectMake(TopFrame.bounds.size.width-55, 0, 50, 50);
  voiceTalkImageView.image = [UIImage imageNamed:@"/Library/BawAppie/KakaoHeads/phone"];
  UITapGestureRecognizer *voiceTalkTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(voiceTalkTap)];
  voiceTalkTap.numberOfTapsRequired = 1;
  [voiceTalkImageView setUserInteractionEnabled:YES];
  [voiceTalkImageView addGestureRecognizer:voiceTalkTap];
  [TopFrame addSubview:voiceTalkImageView];

  NSString* openApplication = [chatHeadsController package:chatID];
  UILabel *displayText = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, view.bounds.size.width - 40, view.bounds.size.height - 100)];
  displayText.font = [UIFont systemFontOfSize:17.0];
  displayText.numberOfLines = 0;
  displayText.textColor = [UIColor colorWithWhite:0.2 alpha:1.0];
  displayText.textAlignment = NSTextAlignmentCenter;
  NSMutableAttributedString *strText = [[NSMutableAttributedString alloc] initWithString:@"Starting KakaoHeads.\nApplication is starting in the background. Please wait.."];
  [strText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Helvetica-Bold" size:22] range:NSMakeRange(0, 20)];
  displayText.attributedText = strText;
  [view addSubview:displayText];

  UIView *contextView = [CDTContextHostProvider viewFor:openApplication];
  contextView.clipsToBounds=YES;
  CGRect contextViewFrame = contextView.frame;
  contextViewFrame.origin.y = -85.0;
  contextView.frame = contextViewFrame;
  [contextView setBackgroundColor:[UIColor clearColor]];
  [view addSubview:contextView];
  [view addSubview:TopFrame];

  self.AppView = view;
  return view;
}

-(void)ChatButtonTap {
  [ChatHeadsController collapseChatHeads];

  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
    [[UIApplication sharedApplication] launchApplicationWithIdentifier:@"com.iwilab.KakaoTalk" suspended:NO];
  });
}
-(void)voiceTalkTap {
  HBLogDebug(@"chatID is %@", [[ChatHeadsController getActiveChatHead] chatID]);
  NSDictionary *userInfo = @{ @"chatID": [[ChatHeadsController getActiveChatHead] chatID] };
  [[objc_getClass("NSDistributedNotificationCenter") defaultCenter] postNotificationName:@"com.rpgfarm.kakaoheadsplus.kakaotalk.voicecall" object:nil userInfo:userInfo];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
  return UIStatusBarStyleLightContent;
}

- (UIImage *)imageWithRoundedCornersSize:(float)cornerRadius usingImage:(UIImage *)original {
  CGRect frame = CGRectMake(0, 0, original.size.width, original.size.height);
  UIGraphicsBeginImageContextWithOptions(original.size, NO, original.scale);
  [[UIBezierPath bezierPathWithRoundedRect:frame cornerRadius:cornerRadius] addClip];
  [original drawInRect:frame];
  UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return image;
}

@end

int main(int argc, char *argv[]) {
  [FCViewController sharedInstance];
  [[NSRunLoop currentRunLoop] run];
}
