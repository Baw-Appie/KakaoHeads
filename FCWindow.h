#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface FCWindow : UIWindow
+ (instancetype)sharedInstance;
- (id)init;
@end
