#import "CDTContextHostProvider.h"
#import "Interfaces.h"

@implementation CDTContextHostProvider

+(SBApplication *)app:(NSString *)bundleID {
    return [[NSClassFromString(@"SBApplicationController") sharedInstance] applicationWithBundleIdentifier:bundleID];
}

+(UIView *)viewFor:(NSString *)bundleID {
    SBApplication *app = [self app:bundleID];
    [self launch:bundleID];
    [self forceBackgrounded:NO forApp:app];
    FBSceneHostManager *manager = [[app mainScene] hostManager];
    [manager enableHostingForRequester:@"KakaoHeads" orderFront:YES];
    return [manager hostViewForRequester:@"KakaoHeads" enableAndOrderFront:YES];
}

+(void)launch:(NSString *)bundleID {
    [[UIApplication sharedApplication] launchApplicationWithIdentifier:bundleID suspended:YES];
}

+(void)forceBackgrounded:(BOOL)backgrounded forApp:(SBApplication *)app {
    FBSMutableSceneSettings *sceneSettings = [[[app mainScene] mutableSettings] mutableCopy];
    [sceneSettings setBackgrounded:backgrounded];
    [(FBScene *)[app mainScene] updateSettings:sceneSettings withTransitionContext:nil completion:nil];
}

+(void)rehost:(NSString *)bundleID {
    SBApplication *app = [self app:bundleID];
    [self launch:bundleID];
    [self forceBackgrounded:NO forApp:app];
    FBSceneHostManager *manager = [[app mainScene] hostManager];
    [manager enableHostingForRequester:@"KakaoHeads" orderFront:YES];
}

+(void)dehost:(NSString *)bundleID {
    SBApplication *app = [self app:bundleID];
    [self forceBackgrounded:YES forApp:app];
    FBSceneHostManager *manager = [[app mainScene] hostManager];
    [manager disableHostingForRequester:@"KakaoHeads"];
}

@end
