TARGET = iphone:9.2:8.0
ARCHS = arm64 armv7
OS := $(shell uname)
ifeq ($(OS),Darwin)
  ARCHS += arm64e
endif

include $(THEOS)/makefiles/common.mk

TWEAK_NAME = KakaoHeads
KakaoHeads_FILES = Tweak.xm FCViewController.m FCWindow.m $(wildcard includes/FCChatHeads/*.*m) $(wildcard includes/CMPopTipView/*.*m) CDTContextHostProvider.mm
KakaoHeads_FRAMEWORKS = UIKit Foundation CoreGraphics
KakaoHeads_LIBRARIES = Rocketbootstrap MobileGestalt
KakaoHeads_PRIVATE_FRAMEWORKS = AppSupport
KakaoHeads_LDFLAGS += -lpop

include $(THEOS_MAKE_PATH)/tweak.mk

SUBPROJECTS += MHKakaoTalkSupport
include $(THEOS_MAKE_PATH)/aggregate.mk

after-install::
	install.exec "killall -9 SpringBoard"
